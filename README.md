# IF3250_2022_33_JADWAL_BACKEND

## How to run

### Swagger
1. `go mod init jadwal` di root
2. `go run ./src/main.go` di root
3. Open link `http://<"your-host">:8090/swagger/index.html` on your browser
4. Swagger API Documentation ready to use

### Docker

1. Install docker
2. Run `docker-compose up`
3. If build is successful, the API endpoint is at `http://localhost:8090`
4. To access **pgAdmin**, open it at `http://localhost:5050`

### Air

1. Install air using `go get github.com/cosmtrek/air`
2. Go to root directory and run `air`

### Terminal

1. initially on root, run `go mod init jadwal`
2. run `go run ./src/main.go`

### Testing

1. on root, run `go test ./...` or `go test ./... -v` to show unit testing's informations
2. to prevent unintended behavior, it's encouraged to run `go mod init jadwal` again on the root directiory

## REST API Endpoints

### Root

#### Request: Root

`GET localhost:8090/`

#### Response: Root

    {
        "message": "API is working properly"
    }

### Register

#### Request: Register

`POST localhost:8090/register`

    {
        "namaLengkap": "Nama Lengkap User",
        "username": "Username user",
        "email": "user@email.com",
        "password": "password"
    }

#### Response: Register

##### Register Good Response

    {
        "data": {
            "namaLengkap": "Nama Lengkap User",
            "username": "Username user",
            "email": "user@email.com",
            "password": "" // Empty
        },
        "status": "OK",
        "token": "JWT.Token"
    }

##### Register Bad Response

    status code: 409
    {
        "status": "Conflict: User already exists"
    }

### Login

#### Request: Login

`POST localhost:8090/login`

    {
        "attribute": "username/email",
        "password": "password"
    }

#### Response: Login

##### Login Good Response

    {
        "data": {
            "namaLengkap": "Nama Lengkap User",
            "username": "Username user",
            "email": "user@email.com",
            "password": ""
        },
        "status": "OK",
        "token": "JWT.Token"
    }

##### Login Bad Response 1 (User not found)

    status code: 403
    {
        "status": "Error: User not found"
    }

##### Login Bad Response 2 (Password mismatch)

    status code: 401
    {
        "status": "Error: Wrong password"
    }

### User

#### Request User

`GET localhost:8090/user`

    [Header]    Authorization: Bearer JWT.Token

#### Good Response User

    {
        "email": "user@email.com",
        "username": "username"
    }

#### Bad Response User

    {
        "message": "No token"
    }

### Users Endpoint

#### Bad Request

`REQUEST localhost:8090/users/:username/*`

    [Header]    Authorization: Bearer JWT.Token

    {
        <request_body>
    }

    *mismatch token identity with endpoint :username

#### Bad Response

    {
        "message": "Invalid token identity"
    }

### Add Events

#### Request Add Events

`POST localhost:8090/users/:username/events`

    [Header]    Authorization: Bearer JWT.Token

    {
        "title": "Example Title",
        "description": "Example Description",
        "scheduled_at": "2022-03-19T15:00:00.000000Z",
        "scheduled_until": "2022-03-19T16:00:00.000000Z",
        "color": 16777215,
        "allday": 0
    }

    *datetime format: ISO8601

#### Good Response Add Events

    {
        "data": {
            "event_id": <event_id>,
            "title": "Example Title",
            "description": "Example Description",
            "scheduled_at": "2022-03-19T15:00:00.000000Z",
            "scheduled_until": "2022-03-19T16:00:00.000000Z",
            "color": 16777215,
            "allday": 0
        },
        "status": "OK"
    }

    *datetime format: ISO8601

#### Bad Response Add Events

    status code: 409
    {
        "status": "Conflict: Identic Event already exists"
    }

### Remove Events

#### Request Remove Events

`DELETE localhost:8090/users/:username/events`

    [Header]    Authorization: Bearer JWT.Token

    {
        "title": "Example Title",
        "description": "Example Description",
        "scheduled_at": "2022-03-19T15:00:00.000000Z",
        "scheduled_until": "2022-03-19T16:00:00.000000Z",
        "color": 16777215,
        "allday": 0
    }

    *datetime format: ISO8601

#### Good Response Remove Events

    {
        "data": {
            "event_id": <event_id>,
            "title": "Example Title",
            "description": "Example Description",
            "scheduled_at": "2022-03-19T15:00:00.000000Z",
            "scheduled_until": "2022-03-19T16:00:00.000000Z",
            "color": 16777215,
            "allday": 0
        },
        "status": "OK"
    }

    *datetime format: ISO8601

#### Bad Response Remove Events

    status code: 404
    {
        "status": "Conflict: Target Event does not exist"
    }

### Edit Event

#### Request Edit Event

`PUT localhost:8090/users/:username/events?event-id=18`

    [Header]    Authorization: Bearer JWT.Token

    {
        "title":"Edited_Test_Title",
        "description":"Edited_description",
        "scheduled_at":"2022-03-19T15:00:00.000000Z",
        "scheduled_until":"2022-03-19T16:00:00.000000Z",
        "color": 16777215,
        "allday": 0
    }

    *datetime format: ISO8601

event-id=18 could be change according to client's event-id request

#### Good Response Edit Event

    {
        "data": {
            "event_id": 18,
            "title": "Edited_Test_Title",
            "description": "Edited_description",
            "scheduled_at": "2022-03-19T15:00:00.000000Z",
            "scheduled_until": "2022-03-19T16:00:00.000000Z",
            "color": 16777215,
            "allday": 0
        },
        "message": "Event updated successfully.",
        "status": "OK"
    }

    *datetime format: ISO8601

#### Bad Response Edit Event

    {
        "message": "Conflict: Target Event does not exist",
        "status": "404 Not Found"
    }
