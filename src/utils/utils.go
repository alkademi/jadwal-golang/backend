package utils

import (
	"errors"
	"strings"

	"jadwal/src/models"
	"jadwal/src/types"
	"net/mail"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Database Query: Returning User's Data based on email or username
func UserData(attribute string, typeEmail bool, withPass bool) *types.USER  {
	var searchUser models.User
	var err error
	if (typeEmail) {
		err = models.DB.First(&searchUser, &models.User{Email: attribute}).Error
	} else {
		err = models.DB.First(&searchUser, &models.User{Username: attribute}).Error
	}
	// Record not found, returning nil
	if (errors.Is(err, gorm.ErrRecordNotFound)) {
		return nil
	}

	if (withPass) {
		return &types.USER{
			NamaLengkap: searchUser.Nama_Lengkap,
			Username: searchUser.Username,
			Email: searchUser.Email,
			Password: searchUser.Password}
	} else {
		return &types.USER{
			NamaLengkap: searchUser.Nama_Lengkap,
			Username: searchUser.Username,
			Email: searchUser.Email}
	}
}

// Database Query: Returning User's ID based on email or username
func UserID(attribute string) *int {
	var searchID models.User
	var err error
	if (ValidEmail(attribute)) {
		err = models.DB.First(&searchID, &models.User{Email: attribute}).Error
	} else {
		err = models.DB.First(&searchID, &models.User{Username: attribute}).Error
	}
	// Record not found, returning nil
	if (errors.Is(err, gorm.ErrRecordNotFound)) {
		return nil
	}
	return &searchID.User_ID
}

// Database Query: return User object
func GetUser(attribute string) *models.User {
	var search models.User
	var err error
	if (ValidEmail(attribute)) {
		err = models.DB.First(&search, &models.User{Email: attribute}).Error
	} else {
		err = models.DB.First(&search, &models.User{Username: attribute}).Error
	}
	// Record not found, returning nil
	if (errors.Is(err, gorm.ErrRecordNotFound)) {
		return nil
	}
	return &search
}

// Remove Password from JSON Object
func ConvertData(input *types.USER) *types.USER {
	return &types.USER{
		NamaLengkap: input.NamaLengkap,
		Username: input.Username,
		Email: input.Email}
}

// Email validation
func ValidEmail(email string) bool {
    _, err := mail.ParseAddress(email)
    return err == nil
}

// Get route paths from *gin.Context
func GetVars(c *gin.Context) []string {
	return strings.Split(c.Request.URL.String(), "/")
}