package main

import (
	"fmt"
	"strconv"

	"jadwal/src/controllers"
	"jadwal/src/docs"
	auth "jadwal/src/middlewares"
	"jadwal/src/models"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// Setup environment
var errEnv error = godotenv.Load(".env")

// @title Swagger Jadwal
// @version 1.0
// @description Dokumentasi API Jadwal.id.

// @BasePath /api/v1

// @securityDefinitions.apikey bearerAuth
// @in header
// @name Authorization
func main() {
	fmt.Println("REST API is running, Debugging mode: " + strconv.FormatBool(gin.IsDebugging()))

	// Opening connection to Database
	models.ConnectDB()

	// Create tables
	models.DB.AutoMigrate(&models.User{}, &models.Event{}, &models.Reminder{}, &models.TokenStore{})

	r := gin.Default()

	swaggerRoutes := r.Group(docs.SwaggerInfo.BasePath)
	{

		root := swaggerRoutes.Group("/")
		{
			root.GET("", controllers.Root)
		}
		register := swaggerRoutes.Group("/register")
		{
			register.POST("", controllers.Register)
		}
		login := swaggerRoutes.Group("/login")
		{
			login.POST("", controllers.Login)
		}
		token := swaggerRoutes.Group("/token")
		{
			token.POST("", controllers.Token)
		}
		logout := swaggerRoutes.Group("/logout")
		{
			logout.POST("", controllers.Logout)
		}
		user := swaggerRoutes.Group("/user", auth.AuthMiddleware)
		{
			user.GET("", controllers.User)
		}
		events := swaggerRoutes.Group("/users/:username/events", auth.AuthMiddleware, auth.UsersMiddleware)
		{
			events.GET("", controllers.GetEvents)
			events.POST("", controllers.AddEvents)
			events.PUT("", controllers.EditEvent)
			events.DELETE("", controllers.RemoveEvents)
		}
		reminders := swaggerRoutes.Group("/users/:username/reminders", auth.AuthMiddleware, auth.UsersMiddleware)
		{
			reminders.GET("", controllers.GetReminders)
			reminders.POST("", controllers.AddReminders)
			reminders.PUT("", controllers.EditReminder)
			reminders.DELETE("", controllers.RemoveReminders)
		}
		settings := swaggerRoutes.Group("/users/:username/reset-password", auth.AuthMiddleware, auth.UsersMiddleware)
		{
			settings.PUT("", controllers.ResetPassword)
		}
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.Run(":80")
}
