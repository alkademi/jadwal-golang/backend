package controllers

import (
	"jadwal/src/models"
	"jadwal/src/types"
	"jadwal/src/utils"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// Paths Information

// ResetPassword godoc
// @Summary Reset Password
// @Description Reset User's Password
// @Tags Reset Password
// @Produce json
// @Param username path int true "Username"
// @Param password body types.RESETPASSWORD true "Reset Password Body"
// @Success 200 {object} types.SWAGGER200RESETPASSWORD
// @Failure 404 {object} types.SWAGGER404RESETPASSWORD
// @Router /users/{username}/reset-password [put]
func ResetPassword(c *gin.Context) {
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var ResetPasswordJSON types.RESETPASSWORD
	c.BindJSON(&ResetPasswordJSON)

	var newPassword = ResetPasswordJSON.Password

	bytes, _ := bcrypt.GenerateFromPassword([]byte(newPassword), 14)
	hashedPass := string(bytes)

	var updatedUser = &models.User{
		Password: string(hashedPass),
	}

	result := models.DB.Where("user_id = ?", id).Updates(&updatedUser)

	if result.RowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "Conflict: Target User does not exist",
			"status":  "404 Not Found",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Password updated successfully.",
		"status":  "OK",
	})

}
