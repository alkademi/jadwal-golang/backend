package controllers

import (
	auth "jadwal/src/middlewares"
	"jadwal/src/models"
	"jadwal/src/types"
	"jadwal/src/utils"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// Paths Information

// Register godoc
// @Summary Register
// @Description Register a user
// @Tags Register
// @Produce json
// @Param register body types.REGISTER true "Register Body"
// @Success 200 {object} types.SWAGGER200LOGIN
// @Failure 500 {string} string "{"message": "xxxx"}"
// @Failure 503 {string} string "{"status": "Fail to create token"}"
// @Failure 409 {string} string "{"status": "Conflict: User already exists"}"
// @Router /register [post]
// Register feature
func Register(c *gin.Context) {
	var RegisterJSON types.REGISTER
	c.BindJSON(&RegisterJSON)

	bytes, _ := bcrypt.GenerateFromPassword([]byte(RegisterJSON.Password), 14)
	hashedPass := string(bytes)

	result := models.DB.Create(&models.User{
		Nama_Lengkap: RegisterJSON.NamaLengkap,
		Username:     RegisterJSON.Username,
		Password:     string(hashedPass),
		Email:        RegisterJSON.Email})

	if result.Error != nil {
		c.JSON(409, gin.H{"status": "Conflict: User already exists"})
	} else {
		var searchUser *types.USER
		searchUser = utils.UserData(RegisterJSON.Email, true, false) // Never returns nil

		accessToken, err1 := auth.CreateToken(searchUser.Username, searchUser.Email, false)
		refreshToken, err2 := auth.CreateToken(searchUser.Username, searchUser.Email, true)

		if err1 != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err1.Error(),
			})
			c.Abort()
		}
		if err2 != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err2.Error(),
			})
			c.Abort()
		}

		result_token := models.DB.Create(&models.TokenStore{
			Username: RegisterJSON.Username,
			Token:    refreshToken,
		})

		if result_token.Error != nil {
			c.JSON(http.StatusServiceUnavailable, gin.H{"status": "Fail to create token"})
		}

		c.JSON(http.StatusOK, gin.H{
			"status":       "OK",
			"data":         utils.ConvertData(searchUser),
			"accessToken":  accessToken,
			"refreshToken": refreshToken,
		})
	}
}
