package controllers

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	"jadwal/src/models"
	"jadwal/src/types"
	"jadwal/src/utils"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Paths Information

// Get reminders for an event
// GetReminders godoc
// @Summary Get Reminders
// @Description Get User's Reminders
// @Tags Reminders
// @Produce json
// @Param event-id query string false "?event-id="
// @Param username path string true "Username"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEREMINDER
// @Failure 400 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Failure 404 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Router /users/{username}/reminders [get]
// @Security bearerAuth
func GetReminders(c *gin.Context) {
	// params = "?event-id=int" or "?date=date_string", cannot use both
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var event_id, err = strconv.Atoi(c.Query("event-id"))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "Bad Request: Couldn't convert string into integer",
		})
		c.Abort()
		return
	}

	reminders_ret := GetEventReminders(*id, event_id)

	if reminders_ret == nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "Not found",
		})
		c.Abort()
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
		"data":   GetEventReminders(*id, event_id),
	})
}

// Add Reminders feature
// AddReminders godoc
// @Summary Add Reminders
// @Description Add User's Reminders
// @Tags Reminders
// @Produce json
// @Param event-id query string false "?event-id="
// @Param username path string true "Username"
// @Param addReminders body types.EVENT true "AddReminders Body"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEREMINDER
// @Failure 400 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Failure 404 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Router /users/{username}/reminders [post]
// @Security bearerAuth
func AddReminders(c *gin.Context) {
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var ReminderJSON types.REMINDER
	c.BindJSON(&ReminderJSON)

	var eventIDConverted, err = strconv.Atoi(c.Query("event-id")) // event-id params

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "Bad Request: Couldn't convert string into integer",
		})
		c.Abort()
		return
	}

	var found, _ = IsEventIDExist(id, &eventIDConverted)

	if !found {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "Conflict: Target Event does not exist",
		})
		c.Abort()
		return
	}

	var reminder = convertModelReminder(id, eventIDConverted, &ReminderJSON)

	var insertedReminder models.Reminder
	models.DB.Create(reminder)
	models.DB.Last(&insertedReminder, &reminder)

	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
		"data": &types.REMINDER{
			Event_ID:     insertedReminder.Event_ID,
			Reminder_ID:  insertedReminder.Reminder_ID,
			Title:        insertedReminder.Title,
			Scheduled_At: insertedReminder.Scheduled_At.UTC().Format(TimeFormat),
		},
	})
}

// Remove reminders based on param "reminder-id"
// DeleteReminders godoc
// @Summary Delete Reminders
// @Description Delete User's Reminders
// @Tags Reminders
// @Produce json
// @Param event-id query string false "?event-id="
// @Param username path string true "Username"
// @Param deleteReminders body types.EVENT true "DeleteReminders Body"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEREMINDER
// @Failure 400 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Failure 404 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Router /users/{username}/reminders [delete]
// @Security bearerAuth
func RemoveReminders(c *gin.Context) {
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var reminder_id, err = strconv.Atoi(c.Query("reminder-id"))

	var found, reminder = isReminderIDExist(id, &reminder_id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "Bad Request: Couldn't convert string into integer",
		})
		c.Abort()
		return
	}

	if !found { // Checks if sender has the reminder
		c.JSON(http.StatusNotFound, gin.H{
			"status": "Conflict: Target Reminder does not exist",
		})
		c.Abort()
		return
	}

	models.DB.Where("reminder_id = ?", reminder_id).Delete(&reminder) // Deletes Reminder

	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
		"data": &types.REMINDER{
			Event_ID:     reminder.Event_ID,
			Reminder_ID:  reminder.Reminder_ID,
			Title:        reminder.Title,
			Scheduled_At: reminder.Scheduled_At.UTC().Format(TimeFormat),
		},
	})
}

// Edit Reminder Feature
// EditReminder godoc
// @Summary Edit Reminder
// @Description Edit User's Reminder
// @Tags Reminders
// @Produce json
// @Param event-id query string false "?event-id="
// @Param username path string true "Username"
// @Param editReminder body types.EVENT true "EditReminder Body"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEREMINDER
// @Failure 400 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Failure 404 {object} types.SWAGGER404400GETEDITREMOVEREMINDERS
// @Router /users/{username}/reminders [put]
// @Security bearerAuth
func EditReminder(c *gin.Context) {
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var ReminderJSON types.REMINDER
	c.BindJSON(&ReminderJSON)

	reminderID := c.Query("reminder-id")
	//convert to int
	var reminderIDConverted int
	reminderIDConverted, err := strconv.Atoi(reminderID)
	if err != nil {
		c.JSON(500, gin.H{
			"status":  "500 Internal Server Error",
			"message": "Couldn't convert string into integer",
		})
		c.Abort()
		return
	}

	timeAt, _ := time.Parse(TimeFormat, ReminderJSON.Scheduled_At)

	var reminder = &models.Reminder{
		User_ID:      *id,
		Title:        ReminderJSON.Title,
		Scheduled_At: timeAt,
	}

	result := models.DB.Where("reminder_id = ?", reminderIDConverted).Updates(&reminder)

	if result.RowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "Conflict: Target Reminder does not exist",
			"status":  "404 Not Found",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": &types.REMINDER{
			Reminder_ID:  reminderIDConverted,
			Title:        reminder.Title,
			Scheduled_At: reminder.Scheduled_At.Format(TimeFormat),
		},
		"message": "Reminder updated successfully.",
		"status":  "OK",
	})
}

// Gets reminders for events
func GetEventReminders(user_id int, event_id int) []types.REMINDER {
	// Get user Reminders
	var reminders []models.Reminder
	resultReminders := models.DB.Where("user_id = ? AND event_id = ?", user_id, event_id).Order("reminder_id asc").Find(&reminders)

	if resultReminders.RowsAffected == 0 {
		return nil
	}

	var reminders_ret []types.REMINDER
	for _, strct := range reminders {
		reminders_ret = append(reminders_ret, types.REMINDER{
			Event_ID:     strct.Event_ID,
			Reminder_ID:  strct.Reminder_ID,
			Title:        strct.Title,
			Scheduled_At: strct.Scheduled_At.UTC().Format(TimeFormat),
		})
	}

	return reminders_ret
}

// Returns true if exists, false if doesn't
func isReminderIDExist(uid *int, reminder_id *int) (bool, *models.Reminder) {
	var searchReminder models.Reminder
	var err error
	var model = &models.Reminder{
		User_ID:     *uid,
		Reminder_ID: *reminder_id,
	}

	err = models.DB.First(&searchReminder, model).Error
	// Record not found
	if errors.Is(err, gorm.ErrRecordNotFound) || *reminder_id == 0 {
		return false, nil
	}

	return true, &searchReminder
}

func convertModelReminder(uid *int, eid int, reminder *types.REMINDER) *models.Reminder {
	timeAt, _ := time.Parse(TimeFormat, reminder.Scheduled_At)

	var model = &models.Reminder{
		User_ID:      *uid,
		Event_ID:     eid,
		Title:        reminder.Title,
		Scheduled_At: timeAt,
	}

	return model
}
