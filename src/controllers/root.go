package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Paths Information

// Root godoc
// @Summary Root
// @Description Beginning Page
// @Tags Root
// @Produce json
// @Success 200 {object} types.SWAGGER200ROOT
// @Router / [get]
func Root(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "API is working properly",
	})
}
