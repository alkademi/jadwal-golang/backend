package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

func User(c *gin.Context) {
	res, err := http.Get("https://randomuser.me/api/")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error fetching data",
		})
		return
	}

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error fetching data",
		})
		return
	}

	for ok := true; ok; ok = len(responseData) == 0 {
		res, err := http.Get("https://randomuser.me/api/")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"message": "Error fetching data",
			})
			return
		}

		responseData, err = ioutil.ReadAll(res.Body)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"message": "Error fetching data",
			})
			return
		}
	}

	type ResultData struct {
		Results []struct {
			Picture struct {
				Thumbnail string
			}
		}
	}

	obj := ResultData{}
	err = json.Unmarshal(responseData, &obj)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error parsing data",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"username": c.Keys["username"],
		"email":    c.Keys["email"],
		"picture":  obj.Results[0].Picture.Thumbnail,
	})
}
