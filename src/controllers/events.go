package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"jadwal/src/models"
	"jadwal/src/types"
	"jadwal/src/utils"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Time Format: ISO8601 ("2006-01-02T15:04:05.000000Z")
const TimeFormat = "2006-01-02T15:04:05.000000Z"

// Get events and reminders feature
// Paths Information

// GetEvents godoc
// @Summary Get Events
// @Description Get User's Events
// @Tags Events
// @Produce json
// @Param view query string false "view=month"
// @Param startdate query string false "startdate="
// @Param username path string true "Username"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEEVENT
// @Failure 404 {object} types.SWAGGER404400GETEDITREMOVEEVENTS
// @Router /users/{username}/events [get]
// @Security bearerAuth
func GetEvents(c *gin.Context) {
	// Get user id
	var user models.User
	models.DB.Where("username = ?", c.Keys["username"]).Find(&user)

	viewType := c.DefaultQuery("view", "month")
	var viewRange int
	switch viewType {
	case "day":
		viewRange = 0
	case "week":
		viewRange = 6
	case "month":
		viewRange = 41
	case "year":
		year, _, _ := time.Now().Date()
		if year%4 == 0 && year%100 != 0 || year%400 == 0 { // Leap year
			viewRange = 365
		} else {
			viewRange = 364
		}
	}

	startDate := c.Query("startdate")

	// Get user events
	var events []models.Event
	resultEvents := models.DB.Where("user_id = ? AND scheduled_at::date BETWEEN date(?) AND date(?) + ?::integer", user.User_ID, startDate, startDate, viewRange).Find(&events)

	var events_ret []*types.EVENT
	for _, strct := range events {
		events_ret = append(events_ret, &types.EVENT{
			Event_ID:        strct.Event_ID,
			Title:           strct.Title,
			Description:     strct.Description,
			Scheduled_At:    strct.Scheduled_At.UTC().Format(TimeFormat),
			Scheduled_Until: strct.Scheduled_Until.UTC().Format(TimeFormat),
			Color:           strct.Color,
			AllDay:          toInt(strct.AllDay),
			Reminders:       GetEventReminders(user.User_ID, strct.Event_ID),
		})
	}

	arr_holidays := GetHolidays(startDate, viewRange) // array of holiday events
	if arr_holidays != nil && len(arr_holidays) != 0 {
		events_ret = append(events_ret, arr_holidays...) // adding holiday events
	} // no reminders for holidays

	if resultEvents.RowsAffected == 0 && len(arr_holidays) == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "Not found",
		})
		c.Abort()
		return
	} else {
		c.JSON(http.StatusOK, gin.H{
			"status":         "OK",
			"data":    	events_ret,
		})
	}
}

// Get holidays based on timeframe, convert to array of types.EVENT
func GetHolidays(date string, days int) []*types.EVENT {
	const api_address = "https://date.nager.at/api/v3/publicholidays/%d/ID"

	parsed_date, _ := time.Parse("2006-01-02", date)
	date_after := parsed_date.Add(time.Hour * 24 * time.Duration(days))

	url := fmt.Sprintf(api_address, parsed_date.Year())
	holidays := getEventsFrom(url)
	if holidays == nil {
		return nil
	}

	if parsed_date.Year() != date_after.Year() { // if requested two years
		url2 := fmt.Sprintf(api_address, date_after.Year())
		second_holidays := getEventsFrom(url2) // shouldn't not return null
		if second_holidays != nil {            // null-safe adding
			holidays = append(holidays, second_holidays...)
		}
	}

	min_range := parsed_date.Add(-time.Hour) // for inclusive between of parsed_date
	max_range := date_after.Add(time.Hour)   // for inclusive between of date_after

	var holiday_events []*types.EVENT
	for _, element := range holidays {
		time_hday, _ := time.Parse("2006-01-02", element.Date)
		if time_hday.After(min_range) && time_hday.Before(max_range) {
			holiday_events = append(holiday_events, convertHoliday(element))
		}
	}

	return holiday_events
}

// Add Events feature
// AddEvents godoc
// @Summary Add Events
// @Description Add User's Events
// @Tags Events
// @Produce json
// @Param username path string true "Username"
// @Param addEvents body types.EVENT true "AddEvents Body"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEEVENT
// @Router /users/{username}/events [post]
// @Security bearerAuth
func AddEvents(c *gin.Context) {
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var EventJSON types.EVENT
	c.BindJSON(&EventJSON)

	var event = convertModel(id, &EventJSON)

	var insertedEvent models.Event
	models.DB.Create(event) // Add error handling, but shouldn't cause error
	models.DB.Last(&insertedEvent, &event)

	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
		"data": &types.EVENT{
			Event_ID:        insertedEvent.Event_ID,
			Title:           insertedEvent.Title,
			Description:     insertedEvent.Description,
			Scheduled_At:    insertedEvent.Scheduled_At.UTC().Format(TimeFormat),
			Scheduled_Until: insertedEvent.Scheduled_Until.UTC().Format(TimeFormat),
			Color:           insertedEvent.Color,
			AllDay:          toInt(insertedEvent.AllDay),
		},
	})
}

// Remove events based on param "event-id"
// RemoveEvents godoc
// @Summary Remove Events
// @Description Remove User's Events
// @Tags Events
// @Produce json
// @Param event-id query string false "?event-id="
// @Param username path string true "Username"
// @Param removeEvents body types.EVENT true "RemoveEvents Body"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEEVENT
// @Router /users/{username}/events [delete]
// @Security bearerAuth
func RemoveEvents(c *gin.Context) {
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var event_id, err = strconv.Atoi(c.Query("event-id"))

	var found, event = IsEventIDExist(id, &event_id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "Bad Request: Couldn't convert string into integer",
		})
		c.Abort()
		return
	}

	if !found { // Checks if sender has the event
		c.JSON(http.StatusNotFound, gin.H{
			"status": "Conflict: Target Event does not exist",
		})
		c.Abort()
		return
	}

	models.DB.Where("event_id = ?", event_id).Delete(&models.Reminder{})
	models.DB.Where("event_id = ?", event_id).Delete(&event) // Deletes Event

	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
		"data": &types.EVENT{
			Event_ID:        event.Event_ID,
			Title:           event.Title,
			Description:     event.Description,
			Scheduled_At:    event.Scheduled_At.UTC().Format(TimeFormat),
			Scheduled_Until: event.Scheduled_Until.UTC().Format(TimeFormat),
			Color:           event.Color,
			AllDay:          toInt(event.AllDay),
		},
	})
}

// Edit Event Feature
// EditEvent godoc
// @Summary Edit Event
// @Description Edit User's Event
// @Tags Events
// @Produce json
// @Param event-id query string false "?event-id="
// @Param username path string true "Username"
// @Param editEvent body types.EVENT true "EditEvent Body"
// @Success 200 {object} types.SWAGGER200GETADDEDITREMOVEEVENT
// @Router /users/{username}/events [put]
// @Security bearerAuth
func EditEvent(c *gin.Context) {
	var vars = utils.GetVars(c)
	var id = utils.UserID(vars[2])
	var EventJSON types.EVENT
	c.BindJSON(&EventJSON)

	eventID := c.Query("event-id")
	//convert to int
	var eventIDConverted int
	eventIDConverted, err := strconv.Atoi(eventID)
	if err != nil {
		c.JSON(500, gin.H{
			"status":  "500 Internal Server Error",
			"message": "Couldn't convert string into integer",
		})
		c.Abort()
		return
	}

	timeAt, _ := time.Parse(TimeFormat, EventJSON.Scheduled_At)
	timeUntil, _ := time.Parse(TimeFormat, EventJSON.Scheduled_Until)

	var event = &models.Event{
		User_ID:         *id,
		Title:           EventJSON.Title,
		Description:     EventJSON.Description,
		Scheduled_At:    timeAt,
		Scheduled_Until: timeUntil,
		Color:           EventJSON.Color,
		AllDay:          (EventJSON.AllDay != 0),
	}

	var search = &models.Event{
		User_ID: *id,
		Event_ID: eventIDConverted,
	}

	var before models.Event

	models.DB.First(&before, &search)
	result := models.DB.Where("event_id = ?", eventIDConverted).Updates(&event)

	if result.RowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "Conflict: Target Event does not exist",
			"status":  "404 Not Found",
		})
		return
	}

	changed := int(before.Scheduled_At.Sub(timeAt).Minutes())

	if (changed != 0) {
		models.DB.Model(&models.Reminder{}).Where("event_id = ?", 
			eventIDConverted).UpdateColumn("scheduled_at", 
			gorm.Expr("scheduled_at - INTERVAL '" + strconv.Itoa(changed) + " min'"))
	}

	c.JSON(http.StatusOK, gin.H{
		"data": &types.EVENT{
			Event_ID:        eventIDConverted,
			Title:           event.Title,
			Description:     event.Description,
			Scheduled_At:    event.Scheduled_At.Format(TimeFormat),
			Scheduled_Until: event.Scheduled_Until.Format(TimeFormat),
			Color:           event.Color,
			AllDay:          toInt(event.AllDay),
		},
		"message": "Event updated successfully.",
		"status":  "OK",
	})
}

// Gets holidays from holiday api
func getEventsFrom(url string) types.HOLIDAYS {
	response, err := http.Get(url)
	if err != nil {
		return nil
	}

	if response.StatusCode != http.StatusOK {
		return nil
	}

	var holidays types.HOLIDAYS
	err = json.NewDecoder(response.Body).Decode(&holidays)
	return holidays
}

// Converts holiday type from api to event type
func convertHoliday(holiday types.HOLIDAY) *types.EVENT {
	const holiday_tf = "2006-01-02"

	time_h, _ := time.Parse(holiday_tf, holiday.Date)

	return &types.EVENT{
		Event_ID:        -1, // event_id for holidays is -1 (to differentiate)
		Title:           holiday.LocalName,
		Description:     holiday.Name,
		Scheduled_At:    time_h.UTC().Format(TimeFormat),
		Scheduled_Until: time_h.UTC().Format(TimeFormat),
		Color:           4281774971, // holiday event color
		AllDay:          1,
	}
}

// Returns true if exists, false if doesn't
func IsEventIDExist(uid *int, event_id *int) (bool, *models.Event) {
	var searchEvent models.Event
	var err error
	var model = &models.Event{
		User_ID:  *uid,
		Event_ID: *event_id,
	}

	err = models.DB.First(&searchEvent, model).Error
	// Record not found
	if errors.Is(err, gorm.ErrRecordNotFound) || *event_id == 0 {
		return false, nil
	}

	return true, &searchEvent
}

func convertModel(uid *int, event *types.EVENT) *models.Event {
	timeAt, _ := time.Parse(TimeFormat, event.Scheduled_At)
	timeUntil, _ := time.Parse(TimeFormat, event.Scheduled_Until)

	var model = &models.Event{
		User_ID:         *uid,
		Title:           event.Title,
		Description:     event.Description,
		Scheduled_At:    timeAt,
		Scheduled_Until: timeUntil,
		Color:           event.Color,
		AllDay:          (event.AllDay != 0),
	}

	return model
}

func toInt(truth bool) int {
	if truth {
		return 1
	} else {
		return 0
	}
}
