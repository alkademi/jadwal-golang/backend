package controllers

import (
	"fmt"
	auth "jadwal/src/middlewares"
	"jadwal/src/models"
	"jadwal/src/types"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

// Paths Information

// Token godoc
// @Summary Token
// @Description Token Mechanism
// @Tags Token
// @Produce json
// @Param token body types.TOKEN true "Token Body"
// @Success 200 {string} string "{}"
// @Failure 500 {string} string "{}"
// @Failure 403 {string} string "{}"
// @Router /token [post]
func Token(c *gin.Context) {
	var TokenJSON types.TOKEN
	c.BindJSON(&TokenJSON)

	token, err := jwt.Parse(TokenJSON.Token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(os.Getenv("API_SECRET_REFRESH")), nil
	})

	if token != nil && err == nil {
		claims, ok := token.Claims.(jwt.MapClaims)

		if !ok {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{
				"message": "Claims not found",
			})
			return
		}

		username := claims["username"].(string)
		email := claims["email"].(string)

		var token_store models.TokenStore

		result := models.DB.Where("username = ? AND token = ?", username, TokenJSON.Token).Find(&token_store)

		if result.RowsAffected == 0 {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{
				"message": "No records of token found",
			})
			return
		}

		accessToken, err := auth.CreateToken(username, email, false)

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
			})
			c.Abort()
		}

		c.JSON(http.StatusOK, gin.H{
			"status":      "OK",
			"accessToken": accessToken,
		})

	} else {
		result := gin.H{
			"message": "Invalid refresh token",
			"error":   err.Error(),
		}
		c.AbortWithStatusJSON(http.StatusForbidden, result)
		return
	}
}
