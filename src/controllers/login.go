package controllers

import (
 auth "jadwal/src/middlewares"
 "jadwal/src/models"
 "jadwal/src/types"
 "jadwal/src/utils"
 "net/http"

 "github.com/gin-gonic/gin"
 "golang.org/x/crypto/bcrypt"
)

// Paths Information

// Login godoc
// @Summary Login
// @Description Login a user
// @Tags Login
// @Produce json
// @Param login body types.LOGIN true "Login Body"
// @Success 200 {object} types.SWAGGER200LOGIN
// @Failure 401 {object} types.SWAGGER401403LOGIN
// @Failure 403 {object} types.SWAGGER401403LOGIN
// @Router /login [post]
func Login(c *gin.Context) {
	var LoginJSON types.LOGIN
	var searchUser *types.USER
	c.BindJSON(&LoginJSON)

	if utils.ValidEmail(LoginJSON.Attribute) {
		searchUser = utils.UserData(LoginJSON.Attribute, true, true)
	} else {
		searchUser = utils.UserData(LoginJSON.Attribute, false, true)
	}
	if searchUser != nil {
		if bcrypt.CompareHashAndPassword([]byte(searchUser.Password),
			[]byte(LoginJSON.Password)) == nil {
			accessToken, err1 := auth.CreateToken(searchUser.Username, searchUser.Email, false)
			refreshToken, err2 := auth.CreateToken(searchUser.Username, searchUser.Email, true)

			if err1 != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
				"message": err1.Error(),
				})
				c.Abort()
			}
			if err2 != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
				"message": err2.Error(),
				})
				c.Abort()
			}

			result_token := models.DB.Create(&models.TokenStore{
				Username: searchUser.Username,
				Token:    refreshToken,
			})

			if result_token.Error != nil {
				var token_store models.TokenStore
				models.DB.Where("username = ?", searchUser.Username).First(&token_store)
				token_store.Token = refreshToken
				result_update := models.DB.Save(&token_store)
				if result_update.RowsAffected == 0 {
				c.AbortWithStatusJSON(http.StatusServiceUnavailable, gin.H{"status": "Fail to create token"})
				}
			}

			c.JSON(http.StatusOK, gin.H{
				"status":       "OK",
				"data":         utils.ConvertData(searchUser),
				"accessToken":  accessToken,
				"refreshToken": refreshToken,
			})

		} else { // Password mismatch
			c.JSON(401, gin.H{"status": "Error: Wrong password"})
		}
	} else {
		// Status user not found
		c.JSON(403, gin.H{"status": "Error: User not found"})
	}
}