package controllers

import (
	"jadwal/src/models"
	"jadwal/src/types"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Paths Information

// Logout godoc
// @Summary Logout
// @Description Logout a user
// @Tags Logout
// @Produce json
// @Param logout body types.TOKEN true "Logout Body"
// @Success 200 {string} string "{"status":  "OK","message": "Successfully logout",}"
// @Failure 400 {string} string "{"message": "No records of token found"}"
// @Router /logout [post]
func Logout(c *gin.Context) {
	var TokenJSON types.TOKEN
	c.BindJSON(&TokenJSON)

	var token_store models.TokenStore
	result := models.DB.Where("token = ?", TokenJSON.Token).Delete(&token_store)

	if result.RowsAffected == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "No records of token found",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status":  "OK",
		"message": "Successfully logout",
	})
}
