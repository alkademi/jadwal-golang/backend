package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"jadwal/src/controllers"
	auth "jadwal/src/middlewares"
	"jadwal/src/models"
	"jadwal/src/types"
	"jadwal/src/utils"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"golang.org/x/crypto/bcrypt"
)

// Setup environment
// var errTestEnv error = godotenv.Load("../.env.test.local")

// Mock User
var mock_nl = "mocked"
var mock_username = "mockedUsername"
var mock_email = "mocked@email.com"
var mock_password = "mockedPassword"

//Connect to Database for API Unit Testing
func connectDatabaseForTest() {
	// Connect to Database
	models.ConnectDB()

	// Create tables
	models.DB.AutoMigrate(&models.User{}, &models.Event{}, &models.Reminder{}, &models.TokenStore{})
}

//Unit Testing to test dummy login unsuccessful causes by unknown attribute (username/email)
//dummy login data implemented by mocking register
func TestLoginUserNotFound(t *testing.T) {
	connectDatabaseForTest()

	const body_message = "Error: User not found"

	err, _, mock_password, _, _ := initDummyUser(t)

	if (err != nil) {
		return
	}

	type response struct {
		Status string
	}

	r := gin.Default()
	r.POST("/login", controllers.Login)

	login := &types.LOGIN{
		Attribute: "wrong_username", // wrong username/email
		Password: mock_password,
	}

	post_body, _ := json.Marshal(login)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(post_body))
	
	r.ServeHTTP(w, req)

	received := response{}
	json.Unmarshal(w.Body.Bytes(), &received)

	// check whether the status matches
	assert.Equal(t, http.StatusForbidden, w.Code)

	// check whether the response body matches
	assert.Equal(t, body_message, received.Status)

	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
	models.DB.Where("username = ?", mock_username).Delete(&models.TokenStore{})
}

//Unit Testing to test dummy login unsuccessful causes by wrong password
//dummy login data implemented by mocking register
func TestLoginPasswordWrong(t *testing.T) {
	connectDatabaseForTest()

	const body_message = "Error: Wrong password"

	err, mock_username, _, _, _ := initDummyUser(t)

	if (err != nil) {
		return
	}

	type response struct {
		Status string
	}

	r := gin.Default()
	r.POST("/login", controllers.Login)

	login := &types.LOGIN{
		Attribute: mock_username,
		Password: "wrong_password", // wrong password
	}

	post_body, _ := json.Marshal(login)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(post_body))
	
	r.ServeHTTP(w, req)

	received := response{}
	json.Unmarshal(w.Body.Bytes(), &received)

	// check whether the status matches
	assert.Equal(t, http.StatusUnauthorized, w.Code)

	// check whether the response body matches
	assert.Equal(t, body_message, received.Status)

	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
	models.DB.Where("username = ?", mock_username).Delete(&models.TokenStore{})
}

//Unit Testing to test dummy login successful and its Authentication
//dummy login data implemented by mocking register
func TestLoginSuccessful(t *testing.T) {
	connectDatabaseForTest()

	err, mock_username, mock_password, _, _ := initDummyUser(t)

	if (err != nil) {
		return
	}

	type response struct {
		Status string
		Data types.USER
		Access_Token string 	`json:"accessToken"`
		Refresh_Token string	`json:"refreshToken"`
	}

	type response_user struct {
		Email string			`json:"email"`
		Username string			`json:"username"`
	}
	
	login := &types.LOGIN{
		Attribute: mock_username,
		Password: mock_password,
	}

	r := gin.Default()
	r.POST("/login", controllers.Login)
	r.Use(auth.AuthMiddleware)
	r.GET("/user", controllers.User)
	
	post_body, _ := json.Marshal(login)
	
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(post_body))
	
	r.ServeHTTP(w, req)
	
	received := response{}
	json.Unmarshal(w.Body.Bytes(), &received)
	
	// check whether the status matches
	assert.Equal(t, http.StatusOK, w.Code)

	// check whether the data matches
	assert.Equal(t, mock_nl, received.Data.NamaLengkap)
	assert.Equal(t, mock_username, received.Data.Username)
	assert.Equal(t, mock_email, received.Data.Email)
	
	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
	models.DB.Where("username = ?", mock_username).Delete(&models.TokenStore{})
}

func TestAddEvent(t *testing.T) {
	connectDatabaseForTest()

	err, username, _, _, bearer := initDummyUser(t)

	if (err != nil) {
		return
	}

	type response struct {
		Status string
		Data types.EVENT
	}

	r := gin.Default()
	r.Use(auth.AuthMiddleware)
	r.Use(auth.UsersMiddleware)
	r.POST("/users/:username/events", controllers.AddEvents)

	event_to_add := &types.EVENT{
		Title: "Mock Title",
		Description: "Mock Description",
		Scheduled_At: "2022-03-19T15:00:00.000000Z",
		Scheduled_Until: "2022-03-19T15:00:00.000000Z",
		Color: 1,
		AllDay: 0,
	}

	post_body, _ := json.Marshal(event_to_add)

	url := fmt.Sprintf("/users/%s/events", username)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(post_body))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code) // Checks Status Code
	
	received := response{}
	json.Unmarshal(w.Body.Bytes(), &received)
	assert.Equal(t, event_to_add.Title, received.Data.Title) // Checks inserted Data

	models.DB.Where("event_id = ?", received.Data.Event_ID).Delete(&models.Event{})
	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
}

func TestRemoveEvent(t *testing.T) {
	connectDatabaseForTest()

	err, username, _, _, bearer := initDummyUser(t)

	if (err != nil) {
		return
	}

	r := gin.Default()
	r.Use(auth.AuthMiddleware)
	r.Use(auth.UsersMiddleware)
	r.DELETE("/users/:username/events", controllers.RemoveEvents)

	times, _ := time.Parse(controllers.TimeFormat, "2022-03-19T15:00:00.000000Z")

	id := utils.GetUser(mock_username).User_ID

	event_to_add := &models.Event{
		User_ID: id,
		Title: "Mock Title",
		Description: "Mock Description",
		Scheduled_At: times,
		Scheduled_Until: times,
		Color: 1,
		AllDay: false,
	}

	var inserted models.Event
	models.DB.Create(&event_to_add)
	models.DB.Last(&inserted, &event_to_add)

	url := fmt.Sprintf("/users/%s/events?event-id=%d", username, inserted.Event_ID)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", url, nil)
	req.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code) // Checks Status Code
	assert.Equal(t, models.DB.First("event_id = ?", inserted.Event_ID).
		RowsAffected, int64(0)) // Checks if search results in 0 row

	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
}

func TestGetEvents(t *testing.T) {
	connectDatabaseForTest()

	err, username, _, _, bearer := initDummyUser(t)
	id := utils.GetUser(mock_username).User_ID // dummy user id

	if (err != nil) {
		return
	}

	type response struct {
		Status string
		Data []types.EVENT
	}

	r := gin.Default()
	r.Use(auth.AuthMiddleware)
	r.Use(auth.UsersMiddleware)
	r.GET("/users/:username/events", controllers.GetEvents)

	// Adding records to database
	var events_to_add []*models.Event
	start, _ := time.Parse(controllers.TimeFormat, "2022-03-19T15:00:00.000000Z")
	end, _ := time.Parse(controllers.TimeFormat, "2022-03-19T16:00:00.000000Z")

	for i := 0; i < 5; i++ { // adding 5 events
		events_to_add = append(events_to_add, 
			&models.Event{
				User_ID: id,
				Title: fmt.Sprintf("Mock Title_%d", (i + 1)),
				Description: fmt.Sprintf("Mock Description_%d", (i + 1)),
				Scheduled_At: start.Add(time.Hour * 24 * time.Duration(i)),
				Scheduled_Until: end.Add(time.Hour * 24 * time.Duration(i)),
				Color: 1,
				AllDay: false,
			},
		)
	}
	
	models.DB.Create(&events_to_add) // Test this

	url := fmt.Sprintf("/users/%s/events?startdate=2022-03-19", username)

	url_day := fmt.Sprintf((url + "&view=%s"), "day")
	url_week := fmt.Sprintf((url + "&view=%s"), "week")
	url_month := fmt.Sprintf((url + "&view=%s"), "month")
	url_year := fmt.Sprintf((url + "&view=%s"), "year")

	// Testing for day parameter
	w := httptest.NewRecorder()
	req_day, _ := http.NewRequest("GET", url_day, nil)
	req_day.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req_day)
	assert.Equal(t, http.StatusOK, w.Code)
	received_day := response{}
	json.Unmarshal(w.Body.Bytes(), &received_day)
	assert.Equal(t, (len(received_day.Data) >= 1), true) // >= because of holiday events
	
	// Testing for week parameter
	w = httptest.NewRecorder()
	req_week, _ := http.NewRequest("GET", url_week, nil)
	req_week.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req_week)
	assert.Equal(t, http.StatusOK, w.Code)
	received_week := response{}
	json.Unmarshal(w.Body.Bytes(), &received_week)
	assert.Equal(t, (len(received_week.Data) >= 5), true)

	// Testing for month parameter
	w = httptest.NewRecorder()
	req_month, _ := http.NewRequest("GET", url_month, nil)
	req_month.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req_month)
	assert.Equal(t, http.StatusOK, w.Code)
	received_month := response{}
	json.Unmarshal(w.Body.Bytes(), &received_month)
	assert.Equal(t, (len(received_month.Data) >= 5), true)

	// Testing for year parameter
	w = httptest.NewRecorder()
	req_year, _ := http.NewRequest("GET", url_year, nil)
	req_year.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req_year)
	assert.Equal(t, http.StatusOK, w.Code)
	received_year := response{}
	json.Unmarshal(w.Body.Bytes(), &received_year)
	assert.Equal(t, (len(received_year.Data) >= 5), true)

	models.DB.Where("user_id = ?", id).Delete(&models.Event{})
	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
}

func TestEditEvent(t *testing.T) {
	connectDatabaseForTest()

	err, username, _, _, bearer := initDummyUser(t)
	id := utils.GetUser(mock_username).User_ID // dummy user id

	if (err != nil) {
		return
	}

	type response struct {
		Status string
		Data types.EVENT
	}

	r := gin.Default()
	r.Use(auth.AuthMiddleware)
	r.Use(auth.UsersMiddleware)
	r.PUT("/users/:username/events", controllers.EditEvent)

	// Adding event record to database
	start, _ := time.Parse(controllers.TimeFormat, "2022-03-19T15:00:00.000000Z")
	end, _ := time.Parse(controllers.TimeFormat, "2022-03-19T16:00:00.000000Z")

	event_to_add := &models.Event{
		User_ID: id,
		Title: fmt.Sprintf("Mock Title"),
		Description: fmt.Sprintf("Mock Description"),
		Scheduled_At: start,
		Scheduled_Until: end,
		Color: 1,
		AllDay: false,
	}

	var inserted models.Event
	models.DB.Create(&event_to_add)
	models.DB.Last(&inserted, &event_to_add)
	event_id := inserted.Event_ID

	event_to_edit := &types.EVENT{
		Title: "Edited Title",
		Description: "Edited Description",
		Scheduled_At: "2022-03-19T15:00:00.000000Z",
		Scheduled_Until: "2022-03-19T15:00:00.000000Z",
		Color: 2,
		AllDay: 1,
	}

	post_body, _ := json.Marshal(event_to_edit)

	url := fmt.Sprintf("/users/%s/events?event-id=%d", username, event_id)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(post_body))
	req.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
	received := response{}
	json.Unmarshal(w.Body.Bytes(), &received)
	assert.Equal(t, received.Data.Title, event_to_edit.Title)
	assert.Equal(t, received.Data.Description, event_to_edit.Description)
	assert.Equal(t, received.Data.Scheduled_At, event_to_edit.Scheduled_At)
	assert.Equal(t, received.Data.Scheduled_Until, event_to_edit.Scheduled_Until)
	assert.Equal(t, received.Data.Color, event_to_edit.Color)
	assert.Equal(t, received.Data.AllDay, event_to_edit.AllDay)

	var edited_event models.Event
	models.DB.Last(&edited_event, &models.Event{})

	assert.Equal(t, edited_event.Title, "Edited Title")
	assert.Equal(t, edited_event.Description, "Edited Description")
	assert.Equal(t, edited_event.Color, 2)
	assert.Equal(t, edited_event.AllDay, true)

	models.DB.Where("user_id = ?", id).Delete(&models.Event{})
	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
}

func TestAddReminder(t *testing.T) {
	connectDatabaseForTest()

	err, username, _, _, bearer := initDummyUser(t)
	id := utils.GetUser(mock_username).User_ID // dummy user id

	if (err != nil) {
		return
	}

	type response struct {
		Status string
		Data types.REMINDER
	}

	r := gin.Default()
	r.Use(auth.AuthMiddleware)
	r.Use(auth.UsersMiddleware)
	r.POST("/users/:username/reminders", controllers.AddReminders)

	// Adding event record to database
	start, _ := time.Parse(controllers.TimeFormat, "2022-03-19T15:00:00.000000Z")
	end, _ := time.Parse(controllers.TimeFormat, "2022-03-19T16:00:00.000000Z")

	event_to_add := &models.Event{
		User_ID: id,
		Title: fmt.Sprintf("Mock Title"),
		Description: fmt.Sprintf("Mock Description"),
		Scheduled_At: start,
		Scheduled_Until: end,
		Color: 1,
		AllDay: false,
	}

	var inserted models.Event
	models.DB.Create(&event_to_add)
	models.DB.Last(&inserted, &event_to_add)
	event_id := inserted.Event_ID

	reminder_to_add := &types.REMINDER{
		Title: "Mock Reminder Title",
		Scheduled_At: start.Add(time.Duration(-15) * time.Minute).Format(
			controllers.TimeFormat),
	}

	post_body, _ := json.Marshal(reminder_to_add)

	url := fmt.Sprintf("/users/%s/reminders?event-id=%d", username, event_id)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(post_body))
	req.Header.Add("Authorization", bearer)

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
	received := response{}
	json.Unmarshal(w.Body.Bytes(), &received)
	assert.Equal(t, received.Data.Title, reminder_to_add.Title)
	assert.Equal(t, received.Data.Scheduled_At, reminder_to_add.Scheduled_At)

	models.DB.Where("user_id = ?", id).Delete(&models.Reminder{})
	models.DB.Where("user_id = ?", id).Delete(&models.Event{})
	models.DB.Where("Nama_Lengkap = ?", mock_nl).Delete(&models.User{})
}

func initDummyUser(t * testing.T) (*error, string, string, string, string) {	
	bytes, _ := bcrypt.GenerateFromPassword([]byte(mock_password), 14)
	hashedPass := string(bytes)
	
	//mocked Register
	result := models.DB.Create(&models.User{
		Nama_Lengkap: mock_nl,
		Username:     mock_username,
		Password:     string(hashedPass),
		Email:        mock_email})
	
	token, _ := auth.CreateToken(mock_username, mock_email, false)
	bearer := "Bearer " + token

	if (result.Error != nil) {
		fmt.Println(result.Error)
		fmt.Println("Failed to initialize dummy user")
		assert.Equal(t, result.Error, nil)
		return &result.Error, mock_username, mock_password, hashedPass, bearer
	}

	return nil, mock_username, mock_password, hashedPass, bearer
}