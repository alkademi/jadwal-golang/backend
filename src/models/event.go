package models

import "time"

type Event struct {
	Event_ID        int `gorm:"autoIncrement;primaryKey"`
	User_ID         int
	Title           string
	Description     string
	Scheduled_At    time.Time
	Scheduled_Until time.Time
	Color           int
	AllDay          bool       `gorm:"type:bool;default:false"`
	Reminders       []Reminder `gorm:"foreignKey:Event_ID;constraint:OnDelete:CASCADE;"`
}
