package models

import (
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDB() {
	// Connecting to Database
	var dsn string = ("host=" + os.Getenv("DB_HOST") + 
	" user=" + os.Getenv("DB_USER") + " password=" + os.Getenv("DB_PASSWORD") + 
	" dbname=" + os.Getenv("DB_NAME") + " port=" + os.Getenv("DB_PORT") + " sslmode=prefer")
	
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	DB = db
}