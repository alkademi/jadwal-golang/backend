package models

type TokenStore struct {
	Username string `gorm:"primaryKey"`
	Token    string `gorm:"unique"`
}