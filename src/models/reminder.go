package models

import "time"

type Reminder struct {
	Reminder_ID  int `gorm:"autoIncrement;primaryKey"`
	User_ID      int
	Event_ID     int
	Title        string
	Scheduled_At time.Time
}
