package models

type User struct {
	User_ID      int `gorm:"autoIncrement;primaryKey"`
	Nama_Lengkap string
	Username     string `gorm:"unique"`
	Password     string
	Email        string     `gorm:"unique"`
	Events       []Event    `gorm:"foreignKey:User_ID;constraint:OnDelete:CASCADE;"`
	Reminders    []Reminder `gorm:"foreignKey:User_ID;constraint:OnDelete:CASCADE;"`
}
