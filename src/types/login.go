package types

type LOGIN struct {
	Attribute string `json:"attribute" binding:"required"`
	Password string `json:"password" binding:"required"`
}