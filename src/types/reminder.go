package types

type REMINDER struct {
	Reminder_ID  int    `json:"reminder_id,omitempty"`
	Event_ID 	 int 	`json:"event_id,omitempty"`
	Title        string `json:"title" binding:"required"`
	Scheduled_At string `json:"scheduled_at" binding:"required"`
}
