package types

type EVENT struct {
	Event_ID        int    		`json:"event_id,omitempty"`
	Title           string 		`json:"title" binding:"required"`
	Description     string 		`json:"description" binding:"required"`
	Scheduled_At    string 		`json:"scheduled_at" binding:"required"`
	Scheduled_Until string 		`json:"scheduled_until" binding:"required"`
	Color           int    		`json:"color"`
	AllDay          int    		`json:"allday"`
	Reminders		[]REMINDER	`json:"reminders,omitempty"`
}
