package types

type TOKEN struct {
	Token string `json:"refreshToken" binding:"required"`
}