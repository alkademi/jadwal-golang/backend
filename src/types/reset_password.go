package types

type RESETPASSWORD struct {
	Password string `json:"password" binding:"required"`
}
