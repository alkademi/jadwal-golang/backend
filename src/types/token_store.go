package types

type TOKEN_STORE struct {
	Username string `json:"username" binding:"required"`
	Token    string `json:"token" binding:"required"`
}