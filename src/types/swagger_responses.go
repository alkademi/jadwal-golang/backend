package types

type SWAGGER200GETADDEDITREMOVEEVENT struct {
	Status string `json:"status"`
	Data   string `json:"data"`
}

type SWAGGER404400GETEDITREMOVEEVENTS struct {
	Status string `json:"status"`
}

type SWAGGER500EDITEVENT struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type SWAGGER200GETADDEDITREMOVEREMINDER struct {
	Status string `json:"status"`
	Data   string `json:"data"`
}

type SWAGGER404400GETEDITREMOVEREMINDERS struct {
	Status string `json:"status"`
}

type SWAGGER500EDITREMINDER struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type SWAGGER404RESETPASSWORD struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type SWAGGER200RESETPASSWORD struct {
	Data    string `json:"data"`
	Status  string `json:"status"`
	Message string `json:"message"`
}

type SWAGGER200ROOT struct {
	Message string `json:"message"`
}

type SWAGGER200LOGIN struct {
	Status       string `json:"status"`
	Data         string `json:"data"`
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

type SWAGGER401403LOGIN struct {
	Status string `json:"status"`
}
