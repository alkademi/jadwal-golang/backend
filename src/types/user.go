package types

type USER struct {
	NamaLengkap string `json:"namaLengkap" binding:"required"`
	Username string `json:"username" binding:"required"`
	Email string `json:"email" binding:"required"`
	Password string `json:"password"`
}