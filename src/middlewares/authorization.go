package middleware

import (
	"fmt"
	"os"
	"strings"
	"time"

	"net/http"

	"github.com/gin-gonic/gin"

	jwt "github.com/golang-jwt/jwt/v4"
)

// Token creation
func CreateToken(username string, email string, refresh bool) (string, error) {
	var token string
	var err error

	claims := jwt.MapClaims{}
	claims["username"] = username
	claims["email"] = email
	if refresh {
		claims["exp"] = 0
	} else {
		claims["exp"] = time.Now().Add(time.Hour).Unix()
	}

	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	if refresh {
		token, err = sign.SignedString([]byte(os.Getenv("API_SECRET_REFRESH")))
	} else {
		token, err = sign.SignedString([]byte(os.Getenv("API_SECRET")))
	}
	if err != nil {
		return "", err
	}
	return token, nil
}

func AuthMiddleware(c *gin.Context) {
	authHeader := c.Request.Header.Get("Authorization")

	if authHeader == "" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"message": "No token",
		})
		return
	}
	
	if !strings.Contains(authHeader, "Bearer") {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"message": "Invalid token",
		})
		return
	}

	tokenString := strings.Replace(authHeader, "Bearer ", "", -1)

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(os.Getenv("API_SECRET")), nil
	})

	if token != nil && err == nil {
		claims, ok := token.Claims.(jwt.MapClaims)

		if !ok {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": "Claims not found",
			})
			return
		}

		if len(c.Keys) == 0 {
			c.Keys = make(map[string]interface{})
		}

		c.Keys["username"] = claims["username"].(string)
		c.Keys["email"] = claims["email"].(string)
		c.Next()
	} else {
		result := gin.H{
			"message": "not authorized",
			"error":   err.Error(),
		}
		c.AbortWithStatusJSON(http.StatusUnauthorized, result)
		return
	}
}