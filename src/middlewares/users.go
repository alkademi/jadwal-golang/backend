package middleware

import (
	"net/http"

	"jadwal/src/utils"

	"github.com/gin-gonic/gin"
)

// If token identity does not match API endpoint
func UsersMiddleware(c *gin.Context) {
	var vars = utils.GetVars(c)

	if c.Keys["username"] != vars[2] {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{ // 403
			"message": "Invalid token identity",
		})
		return
	}
}
